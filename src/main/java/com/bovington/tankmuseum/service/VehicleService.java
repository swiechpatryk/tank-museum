package com.bovington.tankmuseum.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.bovington.tankmuseum.model.Vehicle;
import com.bovington.tankmuseum.repository.VehicleRepository;

@Service
public class VehicleService implements IVehicleService {
	@Autowired
	private VehicleRepository vehicleRepo;

	@Override
	public List<Vehicle> getAllVehicles() {
		return vehicleRepo.findAll();
	}

	@Override
	public Optional<Vehicle> getVehicleById(int id) {
		return vehicleRepo.findById(id);
	}

	@Override
	public void updateVehicle(Vehicle vehicle) {
		vehicleRepo.save(vehicle);

	}

	@Override
	public void addVehicle(String name, int weight, int year) {
		vehicleRepo.save(new Vehicle(name, weight, year));

	}

	@Override
	public void deleteVehicle(int id) {
		Optional <Vehicle> vehicle = vehicleRepo.findById(id);
		if (vehicle.isPresent()) {
			vehicleRepo.delete(vehicle.get());
		}
	}

	@Override
	public void saveVehicle(Vehicle vehicle) {
		vehicleRepo.save(vehicle);
	}

	@Override
	public Page<Vehicle> findPaginated(int pageNr, int pageSize, String sortField, String sortDirection, String searchText){
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() : Sort.by(sortField).descending();
		
		Pageable pageable = PageRequest.of(pageNr - 1, pageSize, sort);
		
		if(searchText == null || searchText == "") {
			return vehicleRepo.findAll(pageable);
		}
		else {
			return vehicleRepo.findByNameContainingIgnoreCase(searchText, pageable);
		}
	}
}
