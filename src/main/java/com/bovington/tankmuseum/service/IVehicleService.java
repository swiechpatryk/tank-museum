package com.bovington.tankmuseum.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.bovington.tankmuseum.model.Vehicle;

public interface IVehicleService {
	List <Vehicle> getAllVehicles();
	Optional <Vehicle> getVehicleById(int id);
	
	void updateVehicle(Vehicle vehicle);
	void addVehicle(String name, int weight, int year);
	void deleteVehicle(int id);
	void saveVehicle(Vehicle vehicle);
	Page <Vehicle> findPaginated(int pageNr, int pageSize, String sortField, String sortDirection, String searchText);
}
