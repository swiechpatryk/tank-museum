package com.bovington.tankmuseum.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle")
public class Vehicle {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String name;
	private int weight;
	private int year;
	
	public Vehicle() {
		super();
	}
	
	public Vehicle(String name, int weight, int year) {
		super();
		this.name = name;
		this.weight = weight;
		this.year = year;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getWeight() {
		return weight;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public Integer getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
}
