package com.bovington.tankmuseum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.bovington.tankmuseum.model.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle, Integer>{
	Page <Vehicle> findByNameContainingIgnoreCase(String name, Pageable pageable);
}
