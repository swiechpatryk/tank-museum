package com.bovington.tankmuseum.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bovington.tankmuseum.model.Vehicle;
import com.bovington.tankmuseum.service.IVehicleService;

@Controller
public class VehicleController {
	@Autowired
	private IVehicleService vehicleService;
	
	@GetMapping("/")
	public String showVehicles(
			@RequestParam(name = "name", required = false) String searchText, 
			ModelMap model) {
		return findPaginated(1, "name", "asc", searchText, model);
	}
	
	@GetMapping("/page/{pageNr}")
	public String findPaginated(@PathVariable(value = "pageNr") int pageNr, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDirection") String sortDirection,
			@RequestParam(name = "name", required = false) String searchText,
			ModelMap model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (!(auth instanceof AnonymousAuthenticationToken)) {
		    return "redirect:/manage-vehicles";
		}
		
		int pageSize = 10;
		
		Page <Vehicle> page = vehicleService.findPaginated(pageNr, pageSize, sortField, sortDirection, searchText);
		List <Vehicle> listVehicles = page.getContent();
		
		model.addAttribute("currentPage", pageNr);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDirection", sortDirection);
		model.addAttribute("reverseSortDirection", sortDirection.equals("asc") ? "desc" : "asc");
		
		model.addAttribute("search", searchText);
		
		model.addAttribute("vehicles", listVehicles);
		
		return "list-vehicles";
	}
	
	@GetMapping("/manage-vehicles")
	public String manageVehicles(
			@RequestParam(name = "name", required = false) String searchText, 
			ModelMap model) {
		return findPaginatedManage(1, "name", "asc", searchText, model);
	}
	
	@GetMapping("/manage-vehicles/page/{pageNr}")
	public String findPaginatedManage(@PathVariable(value = "pageNr") int pageNr, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDirection") String sortDirection,
			@RequestParam(name = "name", required = false) String searchText,
			ModelMap model) {
		int pageSize = 10;
		
		Page <Vehicle> page = vehicleService.findPaginated(pageNr, pageSize, sortField, sortDirection, searchText);
		List <Vehicle> listVehicles = page.getContent();
		
		model.addAttribute("currentPage", pageNr);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDirection", sortDirection);
		model.addAttribute("reverseSortDirection", sortDirection.equals("asc") ? "desc" : "asc");
		
		model.addAttribute("search", searchText);
		
		model.addAttribute("vehicles", listVehicles);
		
		return "manage-vehicles";
	}
	
	@GetMapping("/add-vehicle")
	public String showAddVehiclePage(ModelMap model) {
		model.addAttribute("vehicle", new Vehicle());
		return "vehicle";
	}
	
	@GetMapping("/delete-vehicle")
	public String deleteVehicle(@RequestParam int id) {
		vehicleService.deleteVehicle(id);
		return "redirect:/manage-vehicles";
	}
	
	@GetMapping("/update-vehicle")
	public String showUpdateVehiclePage(@RequestParam int id, ModelMap model) {
		Vehicle vehicle = vehicleService.getVehicleById(id).get();
		model.put("vehicle", vehicle);
		return "vehicle";
	}
	
	@PostMapping("/update-vehicle")
	public String updateVehicle(ModelMap model, @Valid Vehicle vehicle, BindingResult result) {
		if(result.hasErrors()) {
			return "vehicle";
		}
		
		vehicleService.updateVehicle(vehicle);
		return "redirect:/manage-vehicles";
	}
	
	@PostMapping("/add-vehicle")
	public String addVehicle(ModelMap model, @Valid Vehicle vehicle, BindingResult result) {
		if(result.hasErrors()) {
			return "vehicle";
		}
		
		vehicleService.updateVehicle(vehicle);
		return "redirect:/manage-vehicles";
	}
}
