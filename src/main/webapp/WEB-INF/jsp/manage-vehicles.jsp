<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>

<div class="container">
	<div>
		<a type="button" class="btn btn-primary btn-md" href="/add-vehicle">Add Vehicle</a>
	</div>
	<br>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3>List of Vehicles</h3>
		</div>
		<div class="panel-body">
			<form action="/manage-vehicles/">
				<input type="text" name="name"/>
				<input type="submit" value="Search"/>
			</form>
			
			<table class="table table-striped tree-table">
				<thead>
					<tr>
						<th width="40%">
							<a href="/manage-vehicles/page/${currentPage}?sortField=name&sortDirection=${reverseSortDirection}&name=${search}">Name</a>
						</th>
						<th width="20%">
							<a href="/manage-vehicles/page/${currentPage}?sortField=weight&sortDirection=${reverseSortDirection}&name=${search}">Weight</a>
						</th>
						<th width="20%">
							<a href="/manage-vehicles/page/${currentPage}?sortField=year&sortDirection=${reverseSortDirection}&name=${search}">Year</a>
						</th>
						<th width="20%"></th>
					</tr>
				</thead>
				
				<tbody>
					<c:forEach var="vehicle" items="${vehicles}">
						<tr id="row-${vehicle.id}" class="parent-row">
							<td>${vehicle.name}</td>
							<td>${vehicle.weight}</td>
							<td>${vehicle.year}</td>
							<td>
								<a type="button" class="btn btn-success" href="/update-vehicle?id=${vehicle.id}">Update</a>
								<a type="button" class="btn btn-warning" href="/delete-vehicle?id=${vehicle.id}">Delete</a>
							</td>
						</tr>
						
						<tr data-parent="row-${vehicle.id}">
							<td colspan="3">
								<h3>Additional vehicle information</h3>
								Name: ${vehicle.name} <br />
								Weight: ${vehicle.weight} <br />
								Width: ${vehicle.year} <br />
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
 
	<ul class="pagination">
		<c:choose>
			<c:when test="${totalPages > 1}">
				<c:choose>
					<c:when test="${currentPage == 1}">
						<li><span>&#60;</span></li>
					</c:when>
					<c:otherwise>
						<li><a href="/manage-vehicles/page/${currentPage - 1}?sortField=${sortField}&sortDirection=${sortDirection}&name=${search}">&#60;</a></li>
					</c:otherwise>
				</c:choose>
				
				<c:forEach begin="1" end="${totalPages}" step="1"  varStatus="pageStatus">
					<c:choose>
						<c:when test="${currentPage == pageStatus.index}">
							<li><span>${pageStatus.index}</span></li>
						</c:when>
						<c:otherwise>
							<li><a href="/manage-vehicles/page/${pageStatus.index}?sortField=${sortField}&sortDirection=${sortDirection}&name=${search}">${pageStatus.index}</a></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				
				<c:choose>
					<c:when test="${currentPage == totalPages}">
						<li><span>&#62;</span></li>
					</c:when>
					<c:otherwise>
						<li><a href="/manage-vehicles/page/${currentPage + 1}?sortField=${sortField}&sortDirection=${sortDirection}&name=${search}">&#62;</a></li>
					</c:otherwise>
				</c:choose>
			</c:when>
		</c:choose>
	</ul>

</div>

<%@ include file="common/footer.jspf"%>