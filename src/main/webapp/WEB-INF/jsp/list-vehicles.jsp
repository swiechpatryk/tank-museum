<%@ include file="common/header.jspf"%>

<nav role="navigation" class="navbar navbar-default">
	<div class="">
		<a href="/" class="navbar-brand">Bovington Tank Museum</a>
	</div>
	<div class="navbar-collapse">
		<ul class="nav navbar-nav navbar-right">
			<li><a href="/login">Login</a></li>
		</ul>
	</div>
</nav>

<div class="container">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3>Vehicle collection</h3>
		</div>
		<div class="panel-body">
			<form action="/">
				<input type="text" name="name" style="width:89%"/>
				<input type="submit" value="Search" style="width:10%"/>
			</form>
		   
			<table class="table table-striped tree-table">
				<thead>
					<tr>
						<th width="60%"> 
							<a href="/page/${currentPage}?sortField=name&sortDirection=${reverseSortDirection}&name=${search}">Name</a> 
						</th>
						<th width="20%"> 
							<a href="/page/${currentPage}?sortField=weight&sortDirection=${reverseSortDirection}&name=${search}">Weight</a> 
						</th>
						<th width="20%"> 
							<a href="/page/${currentPage}?sortField=year&sortDirection=${reverseSortDirection}&name=${search}">Year</a> 
						</th>
					</tr>
				</thead>
		    
				<tbody>
					<c:forEach var="vehicle" items="${vehicles}">
				     
						<tr id="row-${vehicle.id}" class="parent-row">
							<td>${vehicle.name}</td>
							<td>${vehicle.weight}</td>
							<td>${vehicle.year}</td>
						</tr>
			      
						<tr data-parent="row-${vehicle.id}">
							<td colspan="3">
								<h3>Additional vehicle information</h3>
								Name: ${vehicle.name} <br />
								Weight: ${vehicle.weight} <br />
								Width: ${vehicle.year} <br />
							</td>
						</tr>
		     		</c:forEach>
		    	</tbody>
			</table>
	  	</div>
 	</div>
 
	<ul class="pagination">
		<c:choose> 
			<c:when test="${totalPages > 1}">
				<c:choose>
					<c:when test="${currentPage == 1}">
						<li><span>&#60;</span></li>
					</c:when>
					<c:otherwise>
						<li>
							<a href="/page/${currentPage - 1}?sortField=${sortField}&sortDirection=${sortDirection}&name=${search}"> &#60;</a>
						</li>
					</c:otherwise>
				</c:choose>
	 
				<c:forEach begin="1" end="${totalPages}" step="1"  varStatus="pageStatus">
					<c:choose>
						<c:when test="${currentPage == pageStatus.index}">
							<li><span>${pageStatus.index}</span></li>
						</c:when>
						<c:otherwise>                 
							<li>
								<a href="/page/${pageStatus.index}?sortField=${sortField}&sortDirection=${sortDirection}&name=${search}">${pageStatus.index}</a>
							</li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
	 
				<c:choose>
					<c:when test="${currentPage == totalPages}">
						<li><span>&#62;</span></li>
					</c:when>
					<c:otherwise>                 
		    			<li>
		    				<a href="/page/${currentPage + 1}?sortField=${sortField}&sortDirection=${sortDirection}&name=${search}"> &#62;</a>
		    			</li>
					</c:otherwise>
				</c:choose>
  			</c:when> 
  		</c:choose>
	</ul>
</div>

<%@ include file="common/footer.jspf"%>